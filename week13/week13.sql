select Country, count(CustomerID)
from Customers
group by Country;


select ShipperName, count(Orders.OrderID) as NumberOfOrders
from Shippers join orders on Shippers.ShipperID = orders.ShipperID
group by ShipperName;
order by NumberOfOrders desc;
